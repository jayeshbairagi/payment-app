Payment DApp
=============

Steps for start the client side:
--------------------------------

1. Ensure that a local blockchain instance is running(like Ganache). Run,
```js
npm install -g ganache-cli
ganache-cli
```

2. Ensure you have metamask installed on the browser.

3. To install the dependencies for client.
```js
cd client
npm install
```

4. To deploy the contracts.
```js
truffle migrate --reset
```

5. Login to the owner's Metamask account first to set up the contracts. Confirm the first transaction to set the Token contract address for Users contract.

6. To start the application
```js
npm run dev
```


Steps for start the server:
---------------------------
1. Run mongo docker container and access it:
```
docker run --name some-mongo -d mongo
docker exec -it $(docker ps -f name=mongo -q) bash
```

2. Install server dependencies
```js
cd server
npm install
```

3. To start the server
```js
npm run dev
```


Functionalities to cover:
-------------------------
- Balance updation api
- Update User/Bank onboarding process
- Wallet creation for Users/Banks
- Bank functionalities
- User functionalities
- Complete dockerization of application

Appication overview:
====================
- Blockchain based web application for payments
- Bank, User registrations/whitelisting to participate in the network
- Users can add money from banks in their accounts
- Users can transfer money to other accunts in the network
