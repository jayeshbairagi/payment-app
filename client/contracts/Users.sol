pragma solidity >=0.4.22 <0.6.0;

import "./Token.sol";
import "./Ownable.sol";

contract Users is Ownable{
    struct Bank {
        string bankName;
        bool registered;
    }

    struct User {
        string userName;
        uint userId;
        bool registered;
    }

    mapping(address => Bank) public bank;
    mapping(address => User) public user;
    mapping(address => mapping (address => bool)) public bankUser;
    mapping(address => string) public role;

    Token public tokenController;
    bool public tokenControllerSet;

    modifier isTokenControllerSet() {
        require(tokenControllerSet);
        _;
    }

    function setTokenContract(address newTokenContractAddr) public onlyOwner {
        require(newTokenContractAddr != address(0x0));
        require(!tokenControllerSet);

        tokenController = Token(newTokenContractAddr);
        tokenControllerSet = true;
    }

    function registerBank(string memory bankName, address bankAddress) public onlyOwner isTokenControllerSet {
        require(!bank[bankAddress].registered);

        bank[bankAddress] = Bank(bankName, true);
        role[bankAddress] = "Bank";
    }

    function registerUser(string memory userName, address userAddress, uint userId) public isTokenControllerSet {
        require(bank[msg.sender].registered);
        require(!user[userAddress].registered);

        user[userAddress] = User(userName, userId, true);
        bankUser[msg.sender][userAddress] = true;
        role[userAddress] = "User";
    }

    function allocateFunds(address userAddress, uint funds) public isTokenControllerSet {
        require(bank[msg.sender].registered);
        require(bankUser[msg.sender][userAddress]);

        tokenController.issueTokens(userAddress, funds);
    }

    function withdrawFunds(address userAddress, uint funds) public isTokenControllerSet {
        require(bank[msg.sender].registered);
        require(bankUser[msg.sender][userAddress]);

        tokenController.withdrawTokens(userAddress, funds);
    }

    function transferFunds(address userAddress, uint funds) public isTokenControllerSet {
        require(user[msg.sender].registered);
        require(user[userAddress].registered);
        require(funds > 0);

        tokenController.transfer(msg.sender, userAddress, funds);
    }
}
