pragma solidity >=0.4.22 <0.6.0;

import "./SafeMath.sol";

contract Token {
    event Transfer(address indexed from, address indexed to, uint256 value);

    mapping (address => uint256) balances;
    mapping (address => mapping (address => uint256)) allowed;

    uint256 public totalSupply;
    string constant public NAME = "NPCIToken";
    string constant public SYMBOL = "NPCI";
    uint8 constant public DECIMAL = 18;
    address public usersContract;

    using SafeMath for uint256;

    constructor(address _usersContract) public {
        usersContract = _usersContract;
    }

    modifier onlyFromUsersContract() {
        require(msg.sender == usersContract);
        _;
    }

    function issueTokens(address _to, uint256 _value) public onlyFromUsersContract {
        balances[_to] = balances[_to].add(_value);
        totalSupply = totalSupply.add(_value);
    }

    function withdrawTokens(address _to, uint256 _value) public onlyFromUsersContract {
        balances[_to] = balances[_to].sub(_value);
        totalSupply = totalSupply.sub(_value);
    }

    function transfer(address from, address _to, uint256 _value) public onlyFromUsersContract returns (bool success) {
        require(balances[from] > _value);

        balances[from] = balances[from].sub(_value);
        balances[_to] = balances[_to].add(_value);

        emit Transfer(from, _to, _value);
        return true;
    }

    function balanceOf(address _owner) view public returns (uint256 balance) {
        return balances[_owner];
    }
}
