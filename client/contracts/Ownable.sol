pragma solidity >=0.4.22 <0.6.0;

contract Ownable {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    constructor () public {
        _owner = msg.sender;
        emit OwnershipTransferred(address(0), _owner);
    }

    function owner() public view returns (address) {
        return _owner;
    }

    modifier onlyOwner() {
        require(msg.sender == _owner);
        _;
    }

    function transferOwnership(address newOwner) public {
        require(newOwner != address(0));
        require(msg.sender == _owner);
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
