App = {
  web3Provider: null,
  contracts: {},
  account: "0x0",
  hasVoted: false,
  instances: {},
  role: "",

  // Initialize web3
  initWeb3: async function () {
    if (typeof web3 !== "undefined") {
      // If a web3 instance is already provided by Meta Mask.
      App.web3Provider = web3.currentProvider;
    } else {
      // Specify default instance if no web3 instance provided
      App.web3Provider = new Web3.providers.HttpProvider("http://localhost:8545");
    }
    web3 = new Web3(App.web3Provider);

    // Get the current account address
    web3.eth.getCoinbase(function (err, account) {
      if (err === null) {
        App.account = account;
        $('#accountAddress').html('Your Account: ' + account);
      }
    });
  },

  // Initialize election contract
  initContract: async function () {
    const token = await $.getJSON("Token.json");
    const users = await $.getJSON("Users.json");

    App.contracts.Token = TruffleContract(token);
    App.contracts.Users = TruffleContract(users);

    App.contracts.Token.setProvider(App.web3Provider);
    App.contracts.Users.setProvider(App.web3Provider);

    App.instances.Token = await App.contracts.Token.deployed();
    App.instances.Users = await App.contracts.Users.deployed();

    const userInstance = App.instances.Users;
    App.role = await userInstance.role(App.account);

    const isTokenContractSet = await App.instances.Users.tokenControllerSet();

    if (!isTokenContractSet && App.role === "Owner") {
      await App.instances.Users.setTokenContract(token.networks[web3.currentProvider.networkVersion].address);
    }
  },

  login: async function () {
    const username = $('#username').val();
    const password = $('#password').val();

    try {
      const loginResponse = (await $.ajax({
        method: "post",
        url: "http://localhost:4000/users/login",
        data: {
          username,
          password
        }
      }))
      if (loginResponse.status === 200) {
        localStorage.setItem('token', response.responseJson.token);
      }
    } catch (error) {
      console.log(error);
      App.render();
      $('#errorText').show();
    }
  },

  registerBank: async function () {
    try {
      const token = localStorage.getItem('token');
      const bankName = $('#bankName').val();
      const bankAddress = $('#bankAddress').val();

      const instance = App.instances.Users;
      await instance.registerBank(bankName, bankAddress, { from: App.account });
      await $.ajax({
        method: "post",
        url: "http://localhost:4000/users/register",
        data: {
          username: bankName,
          password: '12345',
          role: 'Bank'
        },
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "Authorization": `Bearer ${token}`,
        }
      })
    } catch (error) {
      console.log(error);
      App.render();
      $('#errorText').show();
    }
  },

  registerUser: async function () {
    try {
      const userName = $('#userName').val();
      const userAddress = $('#usersAddress').val();
      const userId = $('#userId').val();

      const instance = App.instances.Users;
      await instance.registerUser(userName, userAddress, userId, { from: App.account });
      await $.ajax({
        method: "post",
        url: "http://localhost:4000/users/register",
        data: {
          username: userName,
          password: '12345',
          role: 'User'
        },
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "Authorization": `Bearer ${token}`,
        }
      })
    } catch (error) {
      console.log(error);
      App.render();
      $('#errorText').show();
    }
  },

  transferFunds: async function () {
    try {
      const userAddress = $('#userAddress').val();
      const fundAmount = $('#fundAmount').val();

      const instance = App.instances.Users;
      await instance.transferFunds(userAddress, fundAmount, { from: App.account });

      // Ajax request to update funds
    } catch (error) {
      console.log(error);
      App.render();
      $('#errorText').show();
    }
  },

  allocateFunds: async function () {
    try {
      const userAddress = $('#userAddressToAllocateFund').val();
      const fundAmount = $('#allocatedFundAmount').val();

      const instance = App.instances.Users;
      await instance.allocateFunds(userAddress, fundAmount, { from: App.account });

      // Ajax request to update funds
    } catch (error) {
      console.log(error);
      App.render();
      $('#errorText').show();
    }
  },

  listenForTokenEvents: async function () {
    App.instances.Token.allEvents({}, {
      fromBlock: 0,
      toBlock: "latest"
    }).watch(function (error, event) {
      console.log("Token contract event triggered", event);
      App.render();
    });
  },

  listenForUsersEvents: async function () {
    App.instances.Users.allEvents({}, {
      fromBlock: 0,
      toBlock: "latest"
    }).watch(function (error, event) {
      console.log("Users contract event triggered", event);
      App.render();
    });
  },

  render: async function () {
    $("#errorText").hide();

    try {
      const loginContent = $('.login');
      const ownerContent = $('.owner');
      const bankContent = $('.bank');
      const userContent = $('.user');

      loginContent.show();
      ownerContent.hide();
      bankContent.hide();
      userContent.hide();

      if (!!localStorage.getItem('token')) {
        loginContent.hide();
        if (App.role === "Owner") {
          ownerContent.show();
        } else if (App.role === "Bank") {
          bankContent.show();
        } else if (App.role === "User") {
          userContent.show();
        } else {
          $('#unregisteredUserErrorText').show();
        }
        const tokenInstance = App.instances.Token;
        const balance = await tokenInstance.balanceOf(App.account);
        $("#balance").text(balance);
      }
    } catch (error) {
      console.warn(error);
    }
  },
};

$(function () {
  $(window).load(async function () {
    await App.initWeb3();
    await App.initContract();
    await App.render();
    await App.listenForTokenEvents();
    await App.listenForUsersEvents();
  });
});
