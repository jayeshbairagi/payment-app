const SafeMath = artifacts.require("SafeMath.sol");
const Token = artifacts.require("Token.sol");
const Users = artifacts.require("Users.sol");

module.exports = (deployer) => {
  deployer.deploy(SafeMath)
  deployer.link(SafeMath, Token)
  deployer.deploy(Users).then(() => {
    return deployer.deploy(Token, Users.address)
  })
};
